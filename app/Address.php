<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
   protected $table = 'address';
    public $fillable = ['address_book_title','contact_person_name','contact_person_number','address_line_1','address_line_2','pincode','city','state','country'];
}
