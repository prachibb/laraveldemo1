@extends('layouts.default')
 
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Address CRUD</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('addressCRUD.create') }}"> Create New Address</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th width="280px">Action</th>
        </tr>
    @foreach ($addresses as $key => $address)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $address->address_book_title }}</td>
        <td>{{ $address->contact_person_name }}</td>
        <td>{{ $address->contact_person_number }}</td>
        <td>{{ $address->address_line_1 }}</td>
        <td>{{ $address->address_line_2 }}</td>
        <td>{{ $address->pincode }}</td>
        <td>{{ $address->city }}</td>
        <td>{{ $address->state }}</td>
        <td>{{ $address->country }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('addressCRUD.show',$address->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('addressCRUD.edit',$address->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['addressCRUD.destroy', $address->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

    {!! $items->render() !!}

@endsection